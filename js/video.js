import { _$, log } from "./utils"

let iOS_ = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
let iOS = !!navigator.platform && /iPad|iPhone|iPod/.test(navigator.platform);
//BODY LOAD
let boxLoad = _$(".box-wait")

//BOX INFO LAYER
let boxPlayVideo 	= _$("#box-play-video")
let boxReloadVideo  = _$("#box-reload-video")
let boxPreloadVideo = _$("#box-preload-video")

let layerButtonPlay = _$("#layer-button-play")
let iconReload = _$(".icon-reload")

//Button Connect
let buttonConnect   = _$("#button-connect")
let iconBtn   		= _$("#icon-btn")
let progressVideo   = _$(".progress-video")


let removeAllIconsBtn = () => {
	iconBtn.classList.remove("icon-connect")
	iconBtn.classList.remove("icon-pause-black")
	iconBtn.classList.remove("icon-play-black")
}

let toggleAllInfoLayer = (status) => {
	boxPlayVideo.style.display = status
	boxReloadVideo.style.display = status
	boxPreloadVideo.style.display = status
}

let permision		= _$("#permision")

//VIDEO
let video 			= _$("#video")
let isPlay = false;
let videoDuration = 1
let timeSkip = 1
let progreesBar = 0

//LOAD VIDEO 
let loadVideo = ()=>{	
	let videoPlay = () => {

		video.playbackRate = 0.9;

		if ( !permision.hasAttribute("connection") ){
			if(video.paused){
				video.play()
			}else{
				video.pause()	
			}
			toggleAllInfoLayer('none')
		}else{
			//boxLoad.classList.remove('hide-box-fade')
			//boxLoad.style.display = 'inline'
		}

	}

	if(true){ 
		toggleAllInfoLayer("none")
		boxPlayVideo.style.display = 'inline'
	}

	
	layerButtonPlay.addEventListener( 'click', (event) => {
		videoPlay()
	})

	buttonConnect.addEventListener( 'click', (event) => {
		videoPlay()
	})

	boxReloadVideo.addEventListener( 'click', (event) => {
		permision.removeAttribute('connection')
		videoPlay()

	})
	
	video.addEventListener('canplay', (event) =>{
		log('CanPlay')
		videoDuration = video.duration
		timeSkip = ( parseInt(video.getAttribute("time-skip")) == 0 ) ? videoDuration : parseInt(video.getAttribute("time-skip"));
	})

	video.addEventListener('loadedmetadata', (event) => {
		log('loadedmetadata')
		videoDuration = video.duration
     	timeSkip = ( parseInt(video.getAttribute("time-skip")) == 0 ) ? videoDuration : parseInt(video.getAttribute("time-skip"));
	})

	video.addEventListener( 'play', (event) => {
		log('play')
		removeAllIconsBtn()
		toggleAllInfoLayer("none")
		iconBtn.classList.add("icon-pause-black")
	})

	video.addEventListener( 'pause', (event) => {
		log('pause')
		toggleAllInfoLayer("none")
		boxPlayVideo.style.display = 'inline'
		removeAllIconsBtn()
		iconBtn.classList.add("icon-play-black")
	})

	video.addEventListener( 'ended', (event) => {
		log('ended')
		toggleAllInfoLayer("none")
		boxReloadVideo.style.display = 'inline'
		removeAllIconsBtn()
		iconBtn.classList.add("icon-connect")
		progressVideo.style.width = `${ 100 }%`

		permision.setAttribute('connection','ON')

	})

	video.addEventListener( 'waiting', (event) => {
		log('waiting')
		toggleAllInfoLayer("none")
		boxPreloadVideo.style.display = 'inline'
	})


	video.addEventListener('playing', (event)=>{
		log('playing')
		toggleAllInfoLayer("none")
	})

	video.addEventListener('timeupdate', event => {
		log('timeupdate')

		videoDuration = video.duration
		timeSkip = ( parseInt(video.getAttribute("time-skip")) == 0 ) ? videoDuration : parseInt(video.getAttribute("time-skip"));

		boxPlayVideo.style.display = 'none'
		let videoWait = timeSkip

		log(`videoDuration ${videoDuration}`)
		log(`timeSkip ${timeSkip}`)

		iconBtn.textContent = `Conectar en ${ Math.floor( (timeSkip + 1) - video.currentTime) } segundos`
		
		progreesBar = Math.floor( (video.currentTime/timeSkip)*100 )

		if ( progreesBar <= 100){
			progressVideo.style.width = `${ progreesBar }%`
		}
		
		if( video.currentTime >= videoWait ){
			toggleAllInfoLayer("none")
			removeAllIconsBtn()
			iconBtn.classList.add("icon-connect")
			boxPlayVideo.style.display = 'none'
			buttonConnect.classList.add("ani-btn")
			iconReload.classList.add("animate-reload")
			iconBtn.textContent = `Conectar a internet gratis`
			progressVideo.style.width = `${ 100 }%`
			
		}
	})

}


window.onload = () => {
	setTimeout( ()=>{
		boxLoad.classList.add("hide-box-fade")
		setTimeout(  ()=>{ boxLoad.style.display = 'none' }, 1000)
	},2000)

	loadVideo()	
}



