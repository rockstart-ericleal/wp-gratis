
import { _$, log } from "./utils.js"
import UI from './src-game/UI.js';


//import Phaser from 'phaser'
//import gameOptions from 'game-config'
import PlayGame from './src-game/super-ball.js';


// Local vars
let containerGame = _$('.container-box-game');
let linkGameSuperBall = _$('#game-super-ball');
let boxLayerGame = _$('.box-layer-game');


linkGameSuperBall.addEventListener('click', () => {
	 containerGame.style.display = 'inline-block';
	// boxLayerGame.style.display = 'flex';
	// boxLayerGame.classList.add('ani-bounce');

	try {		
		let config = {
			type: Phaser.AUTO,
			backgroundColor: 0x022651,
			scale: {
				mode: Phaser.Scale.FIT,
				autoCenter: Phaser.Scale.CENTER_BOTH,
				parent: "container-game",
				width: window.innerWidth,
				height: window.innerHeight,	
			},
			physics: {
				default: "arcade",
				arcade:{
					// gravity: {
					// 	y:800
					// },
					// debug: true,
				}
			},
			scene: [PlayGame, UI]
		}

		const game = new Phaser.Game(config);
		window.focus();
		

	}
	catch(error) {
	  
	  let el = document.querySelector("body");
	  el.innerHTML = 'err + ' + error;
	}
});


	


window.onerror = function(errorMsg, url, lineNumber) {
	console.log(errorMsg,url,lineNumber);
    let el = document.querySelector("body");
	el.innerHTML = '<h5>'+errorMsg+'</h5><a href="'+url+'">'+url+'</a><h5>'+lineNumber+'</h5>';
};


let boxLoad = _$(".box-wait")
window.onload = () => {
	
	setTimeout( ()=>{
		boxLoad.classList.add("hide-box-fade");
		setTimeout(  ()=>{ boxLoad.style.display = 'none' }, 1000);
	},2000)

}
