import { _$, log } from "../utils.js"
import gameOptions from './game-config.js';


var boxLayerGame = _$('.box-layer-game');
// var startSuperball = _$('#start-superball');
var lifes = 3;

class PlayGame extends Phaser.Scene{
	constructor(){
		super("PlayGame");
	}

	init(){
			
	}

	preload(){

		//Images
		this.load.path = '../../img/assets/game-1/'

		this.load.image("ground", "images/ground.png");
		this.load.image("bgg", "images/bgg.png");
		this.load.image("bg-ad", "ads/ads-1.jpg");
		
		this.load.image('blue', './particles/blue_.png');
		this.load.image('explo-fire', 'particles/fire.png');
		this.load.image("ball", "images/ball.png");
		this.load.image("star", "images/stars.png");
		this.load.image("fire", "images/fire.png");
		//this.load.image("icon_king", "images/icon_king.png");
		
		//Sounds
		this.load.audio('fx-bounce', ['sounds/bounce-ball-1.mp3']);
		this.load.audio('fx-sound-bg', ['sounds/sound-bg.mp3']);
		this.load.audio('fx-fire', ['sounds/incident_game.mp3']);

		//Fonts
		this.load.image('number-font', 'fonts/number-font.png');
		this.load.json('number-font-json','fonts/number-font.json');

		//Ads
		this.load.image('add-1', 'ads/ads-1.jpg');

		



		
		
	}

	create(){

		
		this.templateScore = '000000';	

		//
		this.soundFireActive = false;
		let rnd = new Phaser.Math.RandomDataGenerator((Date.now() * Math.random()).toString());
		this.sound.stopAll();
		//Cameras
		this.camera = this.cameras.main;

		//Sounds
		this.bounceSound = this.sound.add('fx-bounce');
		this.bounceSound.volume = 0.7;
		
		// this.bgSound = this.sound.add('fx-sound-bg');
		// this.bgSound.loop = true;
		// this.bgSound.play();
		// this.bgSound.volume = 0.2;

		this.fireSound = this.sound.add('fx-fire');		

		//Create Group which will contain all platforms
		this.platformGroup = this.physics.add.group();
		this.fireGroup = this.add.group();
		
		this.bgg = this.add.image(0, 0, 'bgg');
		this.bgAd = this.add.image(0, 0,'bg-ad');
		this.bgAd.setDisplaySize(this.scale.width, this.scale.height);
		this.bgAd.setPosition(this.scale.width/2, this.scale.height/2);
		this.bgAd.alpha = 0.8;

		this.starsArray = [];
		for(let s = 0; s < gameOptions.amount_stars; s++){
			let xStar = Phaser.Math.Between(this.game.config.width + 100, window.innerWidth);
			let yStar = Phaser.Math.Between(100, this.game.config.height*2 -30);

			this.starBg = this.add.sprite(xStar, yStar, 'star');
			this.starBg.velocity = 0.2 + rnd.frac() * 2;
			this.starBg.alpha = rnd.frac() - 0.3;
			this.starBg.scale = (0.3 + rnd.frac());
			this.starsArray[s] = this.starBg;
		}

       
		this.bgg.setDisplaySize(this.game.config.width*2,this.game.config.height*2);

		this.ball = this.physics.add.image(this.game.config.width * gameOptions.ballPosition, this.game.config.height * gameOptions.groundPosition - gameOptions.ballHeight, "ball");
		this.ball.body.gravity.y = gameOptions.ballGravity;
		this.ball.setBounce(1);
		this.ball.setScale(0.7);
		this.ball.body.checkCollision.down = true;
        this.ball.body.checkCollision.up = false;
        this.ball.body.checkCollision.left = false;
        this.ball.body.checkCollision.right = false;
        this.ball.setSize(30, 50, true);
        this.ball.body.setCircle(18);
        this.ball.body.setOffset(18, 15);

        //First Platform 
        let platformX = this.ball.x;

        for ( let i = 0; i < 5; i++ ){
        	let platform = this.platformGroup.create(0, 0, "ground" );
        	platform.setImmovable(true);
        	platform.body.setSize(platform.width-20, 50);
        	platform.body.setOffset(10, 10);
        	platform.setScale(0.5);

        	this.placePlatform(i, platform, platformX);
        	platformX += Phaser.Math.Between(gameOptions.platformDistanceRange[0], gameOptions.platformDistanceRange[1]);
        }

        console.log('reload=> ',this.platformGroup);

        this.input.on("pointerdown", this.movePlatforms, this);
        this.input.on("pointerup", this.stopPlatforms, this);

        this.score = 0;

        //NO FUNCIONA EN PORTAL CAUTIVO
        this.topScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : localStorage.getItem(gameOptions.localStorageName);

        this.scoreText = this.add.text(10, 10, "");     
        
        let particles = this.add.particles('blue');
        let emitter = particles.createEmitter({
	        speed: 50,
	        scale: { start: .4, end: 0},
	        blendMode: 'ADD',
	        gravityY: 350,
	        lifespan: 500
		});
	    emitter.startFollow(this.ball);


	    let exploFire = this.add.particles('explo-fire');
	    this.explotion = exploFire.createEmitter({
	    	active:false, 
	    	yoyo: false, 
	    	quantity: 1,
	    	scale: { start: .8, end: 0},
	    });
	    this.explotion.setLifespan(600);
	    this.explotion.setSpeed(200);

		//Events
		this.registry.events.on('start-superball', ()=> {
			log('stat super ball');
			this.ball.body.gravity.y = 1000;
			this.scene.resume('PlayGame');
		});

		this.registry.events.on('start-superball-pause', ()=> {
			log('stat super ball pause');
			this.scene.pause('PlayGame');
		});

		this.scene.launch('UI');

		if(lifes <= 0){
			this.scene.pause('PlayGame');	
			this.registry.events.emit('connet-superball');
		}


		//this.registry.events.emit('set-current-score', lifes);
		//this.updateScore(0);
				
	}

	startGame(){
		//this.scene.start("PlayGame");
	}

	update(){
		let c = this.camera;
		let bs = this.bounceSound;



		this.physics.world.collide(this.platformGroup, this.ball, function(platform, ball){
        	if(ball.fallingDown){
        		ball.body.gravity.y = 1000;
        	}
        	c.shake(50, 0.01)
        	bs.play();	
        	// log(platform.body.bounce.y, this);
        });



        this.platformGroup.getChildren().forEach(function(platform){
        	if (platform.getBounds().right < 0 ){
        		this.updateScore(1);
        		this.placePlatform(1,platform, this.getRightmostPlatform() + Phaser.Math.Between(gameOptions.platformDistanceRange[0], gameOptions.platformDistanceRange[1]) );
        	}
        }, this);
     
        if(this.ball.y > this.game.config.height){         
           	
          	if (this.soundFireActive == false){
          		this.fireSound.play();
          		//NO FUNCIONA EN PORTAL CAUTIVO
	            localStorage.setItem(gameOptions.localStorageName, Math.max(this.score, this.topScore));         
	            this.explotion.setBlendMode(Phaser.BlendModes.ADD);
	            this.explotion.setPosition(this.ball.x, this.game.config.height);
	            this.explotion.active = true;

	            let e = this.explotion;
	   			let es = this;   		
	   			

	   			
	   			lifes = lifes - 1;
	   			this.registry.events.emit('set-current-score', lifes);

	            this.time.delayedCall(1600, function() {			    				   
				    es.registry.events.emit('show-continue-box');
				    //es.start("PlayGame");
				});
          	}
          	this.soundFireActive = true;

            // this.explotion.tint(0xff0000);   			
         
        }


        for(let s = 0; s < gameOptions.amount_stars; s++){						
			let starBg = this.starsArray[s];
			starBg.x -= starBg.velocity;
			if(starBg.x <= -100){
				starBg.x = Phaser.Math.Between(window.innerWidth + 100, window.innerWidth);
				starBg.y = Phaser.Math.Between(100, window.innerHeight*2 -30);
			}
			//log(starBg.x);
		}

	}

	getRightmostPlatform(){
		let rightmostPlatform = 0;
		this.platformGroup.getChildren().forEach(function(platform){
			rightmostPlatform = Math.max(rightmostPlatform, platform.x);
		}, this);
		return rightmostPlatform;
	}

	updateScore(currentScore){
		this.score += currentScore;
		this.registry.events.emit('set-user-score', this.score);
	}

	movePlatforms(event){
		if(event.y >= 100){
			this.platformGroup.setVelocityX(-gameOptions.platformSpeed);	
		}
	}

	stopPlatforms(){
		this.platformGroup.setVelocityX(0);
	}

	placePlatform(index, platform, posX){
		platform.x = posX;
		//Variante de altura de plataformas.
		platform.y = this.game.config.height * gameOptions.groundPosition + Phaser.Math.Between(gameOptions.platformHeightRange[0], gameOptions.platformHeightRange[1]);

		//Permitir caer a la plataforma
		log(posX != this.ball.x && Phaser.Math.Between(1, 100) <= gameOptions.fallingPlatformPercent)
		//platform.fallingDown = posX != this.ball.x && Phaser.Math.Between(1, 100) <= gameOptions.fallingPlatformPercent;
		log('INDEX', index);
		if(index == 0){
			platform.fallingDown = false;
		}else{
			platform.fallingDown = false;
		}

		platform.displayWidth = Phaser.Math.Between(gameOptions.platformLengthRange[0], gameOptions.platformLengthRange[1]);

		platform.body.gravity.y = 0;
		platform.body.velocity.y = 0;
	}

}


export default PlayGame;