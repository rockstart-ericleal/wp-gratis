import { _$, log } from "../utils.js"
var boxLayerGame = _$('.box-layer-game');
var lifesUI= 3;

class UI extends Phaser.Scene {
	constructor(){
		super({key:'UI'});
	}

	init(){
		console.log("Init UI");
		this.scene.moveUp();
	}

	preload(){
		this.load.path = '../../img/assets/game-1/';
		this.load.image('pixel', 'images/pixel.png');
		this.load.image('play', 'images/play-start-ball@2x.png');
		this.load.image('play-yellow', 'images/play-yellow@2x.png');

		//UI
		this.load.image("icon_pausa", "images/icon_pausa.png");
		this.load.image("bg-continue", "images/bg-continue.png");
		//this.load.image("btn_close_ranking", "images/icon_close_ranking.png");

		//UI
		this.load.image('box-point', 'images/box-points.png');
		this.load.image('icon-king', 'images/icon-king.png');
		this.load.image('icon-point', 'images/icon-point.png');
		this.load.image('icon-life', 'images/icon-life.png');

		this.load.image("bg-connect", "images/bg-connect.png");
		this.load.image("ring-load", "images/ring-load.png");

		this.load.image("btn-connect-wifo", "images/btn-connect-wifo.png");

		
		//Sounds
		this.load.audio('fx-click', ['sounds/explosion_shine.mp3']);
	}

	create(){


		let topBoxDistance = 32;
		let box1 = this.add.image(0, 0, 'box-point').setScale(0.5);
		box1.setPosition(box1.displayWidth/2 +10, topBoxDistance);		
		let box2 = this.add.image(box1.x + box1.displayWidth + 10, topBoxDistance, 'box-point').setScale(0.5);
		let box3 = this.add.image(box2.x + box1.displayWidth + 10, topBoxDistance, 'box-point').setScale(0.5);
		// this.add.image(20, 30, 'box-point');

		this.add.image(box1.x - (box1.displayWidth / 2) + 16, box1.y,'icon-king').setScale(0.5);
		this.add.image(box2.x - (box2.displayWidth / 2) + 14, box1.y,'icon-point').setScale(0.5);

		let scoreKing = 123//boxLayerGame.getAttribute("best-score");

		let scoreStyle = { color: '#000', align: 'right', fontFamily: '"Geogrotesque Medium"' };
		this.scoreKing = this.add.text((box1.x + box1.displayWidth/2) -5, box1.y, scoreKing, scoreStyle);
		this.scoreKing.setOrigin(1, 0.5);

		this.scoreUser = this.add.text((box2.x + box2.displayWidth/2) -5, box2.y, "0", scoreStyle);
		this.scoreUser.setOrigin(1, 0.5);


		this.registry.events.on('set-user-score', (score)=>{
			this.scoreUser.text = score;
		});

		this.registry.events.on('set-current-score', (lifes) => {
			if(lifes > 0){
				let dead = this.lifesGroup.getChildren()[lifes];
				dead.setAlpha(0);
			}
			lifesUI= lifes;
			console.log('LIFES:::', lifes);
		});


		this.lifesGroup = this.add.group()
		

		for (let j = 0; j < lifesUI; j++){
			this.lifesGroup.create(0, box3.y, 'icon-life');
		}

		this.lifesGroup.children.iterate( (life) => {
			life.setScale(0.5);
			
		});

		this.lifesGroup.setX(box3.x - 20, 20);
		this.lifesGroup.setOrigin(0);

		
		//UI Play
		this.clickSound = this.sound.add('fx-click');
		
		this.bgMenu = this.add.image(0, 0, 'pixel');
		this.bgMenu.setScale(this.scale.width*2, this.scale.height*2);
		this.bgMenu.alpha = 0.5;

		

		this.platBg = this.add.image(0, 0, 'play');
		this.platBg.setScale(0.5);

		this.play = this.add.image(0, 0, 'play-yellow');
		this.play.setScale(0.5);
		this.play.setInteractive();

		this.add.tween({
			targets:[this.play],				
			duration:500,
			ease:'Back.easeIn',
			scale: .56,
			yoyo:true,
			repeat: -1
		});



		//Pause

		this.iconPausa = this.add.image(0, 0, 'icon_pausa');
		this.iconPausa.setScale(0.3);
		this.iconPausa.setPosition(this.scale.width-28, 28);
		this.iconPausa.setInteractive();


		this.iconPausa.on(Phaser.Input.Events.POINTER_DOWN, ()=>{

			this.clickSound.play();
			this.add.tween({
				targets:[this.tableroContainer],				
				duration:300,
				ease:'Back.easeIn',
				scale: 1,
				onComplete: () =>{
					this.registry.events.emit('start-superball-pause');
				}
			});
		});


		Phaser.Display.Align.In.Center(this.play, this.platBg, 0, -50);

		this.tableroContainer = this.add.container(this.scale.width/2,this.scale.height/2);
		this.tableroContainer.add([	
			this.bgMenu,		
			this.platBg,
			this.play
		]);

		this.play.on(Phaser.Input.Events.POINTER_DOWN, ()=>{
			this.clickSound.play();
			this.add.tween({
				targets:[this.tableroContainer],				
				duration:300,
				ease:'Back.easeIn',
				scale: 0,
				onComplete: () =>{
					this.registry.events.emit('start-superball');
				}
			});
		});


		if(lifesUI < 3){
			this.tableroContainer.setScale(0);
			this.registry.events.emit('start-superball');	
		}


		//UI Pause Continue

		this.bgMenuRanking = this.add.image(0, 0, 'pixel');
		this.bgMenuRanking.setScale(this.scale.width*2, this.scale.height*2);
		this.bgMenuRanking.alpha = 0.5;		

		this.bgContinue = this.add.image(0, 0, 'bg-continue');
		this.bgContinue.setScale(0.5);

		this.playContinue = this.add.image(0, -54, 'play-yellow');
		this.playContinue.setScale(0.4);
		this.playContinue.setInteractive();




		this.btnConnectWifo = this.add.image(0, 165, 'btn-connect-wifo');
		this.btnConnectWifo.setScale(0.5);
		this.btnConnectWifo.setInteractive();

		this.add.tween({
			targets:[this.playContinue],				
			duration:500,
			ease:'Back.easeIn',
			scale: .48,
			yoyo:true,
			repeat: -1
		});


		let userStyle = {color: '#fff', fontFamily: 'Arial', align: 'center', fontFamily: '"Geogrotesque Medium"', fontSize: '27px', };
		this.nameUser = this.add.text(0,54, 'SANTIAG0', userStyle);
		this.nameUser.setOrigin(0.5);

		let maxScoreStyle = {color: '#fff', fontFamily: 'Arial', align: 'center', fontFamily: '"Geogrotesque Medium"', fontSize: '18px', };
		this.maxScore = this.add.text(0,80, 'TU MEJOR SCORE: 234', maxScoreStyle);
		this.maxScore.setOrigin(0.5);


		this.continueContainer = this.add.container(this.scale.width/2,this.scale.height/2);
		this.continueContainer.add([	
			this.bgMenuRanking,		
			this.bgContinue,
			this.playContinue,
			this.btnConnectWifo,
			this.nameUser,
			this.maxScore
			//this.btn_close_ranking
		]);
		this.continueContainer.setScale(0);


		this.playContinue.on(Phaser.Input.Events.POINTER_DOWN, ()=>{
			this.clickSound.play();
			this.add.tween({
				targets:[this.continueContainer],				
				duration:300,
				ease:'Back.easeIn',
				scale: 0,
				onComplete: () =>{
					this.scene.start('PlayGame');
				}
			});
		});

		this.btnConnectWifo.on(Phaser.Input.Events.POINTER_DOWN, ()=>{
			this.clickSound.play();
			this.add.tween({
				targets:[this.continueContainer],				
				duration:300,
				ease:'Back.easeIn',
				scale: 0,
				onComplete: () =>{

					let scene = this;
					this.time.delayedCall(500, function() {			    				   
					    scene.clickSound.play();
						scene.add.tween({
							targets:[scene.connetContainer],				
							duration:300,
							ease:'Back.easeIn',
							scale: 1,
							onComplete: () =>{
								log("Terminate");
							}
						});
					});				
				}
			});
		});

		



		//UI Connet
		this.bgMenuRing = this.add.image(0, 0, 'pixel');
		this.bgMenuRing.setScale(this.scale.width*2, this.scale.height*2);
		this.bgMenuRing.alpha = 0.5;		

		this.bgRing = this.add.image(0, 0, 'bg-connect');
		this.bgRing.setScale(0.5);

		this.ringLoad = this.add.image(0, 32, 'ring-load');
		this.ringLoad.setScale(0.5);		
		
		this.add.tween({
			targets:[this.ringLoad],				
			duration:6000,
			ease:'Linear',
			rotation: 45,
			repeat:-1,
			onComplete: () =>{
				log("Terminate rotate");
			}
		});


		this.connetContainer = this.add.container(this.scale.width/2,this.scale.height/2);
		this.connetContainer.add([	
			this.bgMenuRing,		
			this.bgRing,
			this.ringLoad
		]);
		this.connetContainer.setScale(0);


		//Events
		this.registry.events.on('connet-superball', ()=> {

			this.clickSound.play();
			this.add.tween({
				targets:[this.connetContainer],				
				duration:300,
				ease:'Back.easeIn',
				scale: 1,
				onComplete: () =>{
					log("Terminate");
				}
			});
		});


		//Events
		this.registry.events.on('show-continue-box', ()=> {
			if(lifesUI > 0 ){
				log('stat dialog continue ball');									
				this.clickSound.play();
				this.add.tween({
					targets:[this.continueContainer],				
					duration:300,
					ease:'Back.easeIn',
					scale: 1,
					onComplete: () =>{
						this.scene.pause('PlayGame');				
					}
				});
			}else{
				this.clickSound.play();
				this.add.tween({
					targets:[this.connetContainer],				
					duration:300,
					ease:'Back.easeIn',
					scale: 1,
					onComplete: () =>{
						log("Terminate");
					}
				});
			}
		});

	}
}

export default UI;