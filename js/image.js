import PinchZoom from 'pinch-zoom-js'
import { _$, log } from "./utils.js"


let pageZomm = _$('.page-zoom')
let originalFlyer = _$('#original-flyer')

let buttonConnect = _$("#btn-image-connect")
let timeWait = buttonConnect.getAttribute("wite-time")
let permision	  = _$("#permision")

if ( parseInt(timeWait) == 0 ){

	buttonConnect.removeAttribute("disabled")

}else{

	buttonConnect.setAttribute("disabled","disabled")

	let textButton = _$("#btn-image-connect .text")

	let timeStarConection = parseInt(timeWait)

	let timeInterval = setInterval( ()=>{
		permision.removeAttribute('connection','ON')
		textButton.textContent = `Espera ${timeStarConection} segundos...`
		timeStarConection -= 1
		if(timeStarConection < 0){
			clearInterval(timeInterval)
			buttonConnect.removeAttribute("disabled")
			textButton.textContent = `Conectar a internet gratis`
			textButton.classList.add("ani-btn")
			permision.setAttribute('connection','ON')
		}
	},1000)
}


originalFlyer.addEventListener('load', (eve) =>{
	let urlImage = originalFlyer.getAttribute("src")

	let zoomImage = document.createElement("img")
	zoomImage.setAttribute("src",urlImage)

	let wrapperSubImage = document.createElement("div")
	wrapperSubImage.setAttribute("class","pinch-zoom")
	wrapperSubImage.appendChild(zoomImage)

	let wrapperZoomImage = document.createElement("div")
	let styleWrapper = `width:${originalFlyer.width}px;height:${originalFlyer.height}px`
	wrapperZoomImage.setAttribute("style",styleWrapper)
	wrapperZoomImage.setAttribute("class","page")

	wrapperZoomImage.appendChild(wrapperSubImage)

	pageZomm.appendChild(wrapperZoomImage)
	originalFlyer.style.display = 'none'

	let el = _$('.pinch-zoom');
	new PinchZoom(el,{
	    draggableUnzoomed: false,
	    minZoom: 1,
	    onZoomStart: function(object, event){
	        // Do something on zoom start
	        // You can use any Pinchzoom method by calling object.method()
	    },
	    onZoomEnd: function(object, event){
	        // Do something on zoom end
	    }
	});
})


let boxLoad = _$(".box-wait")
window.onload = () => {
	
	setTimeout( ()=>{
		boxLoad.classList.add("hide-box-fade")
		setTimeout(  ()=>{ boxLoad.style.display = 'none' }, 1000)
	},2000)

}



