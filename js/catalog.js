import Swiper from 'swiper'
import { _$, log } from "./utils.js"

let boxContainerheight = _$(".swiper-slide")
let endSlide = _$(".end-slide")

let btnConetionElement = _$("#btn-connect")
let btnContainer = _$(".container-cn-btn")
let btnConetionText = _$("#btn-connect .text")

let layerInfo = _$(".ct")
let btnLayerInfoClose = _$(".btn-play-video")

let firstImage = _$(".swiper-slide img")

let permision		= _$("#permision")
let boxLoad = _$(".box-wait")

btnLayerInfoClose.addEventListener('click', () => {
	layerInfo.style.display = 'none'
})

// btnContainer.addEventListener('click', () => {
// 	if( btnConetionElement.disabled == true ){
// 		layerInfo.style.display = 'inline'
// 	}
// })

btnContainer.addEventListener('click', ()=>{

	if( btnConetionElement.disabled == true ){
		layerInfo.style.display = 'inline'
		log("NO Internet WG!")
	}else{
		log("Connect to Internet WG!")
		boxLoad.classList.remove('hide-box-fade')
		boxLoad.style.display = 'inline'
	}
})

let swiper = new Swiper ('.swiper-container', {
	// Optional parameters
	// direction: 'vertical',
	spaceBetween: -29,
	
	// If we need pagination
	pagination: {
		el: '.swiper-pagination',
	},

	on: {
		init: function () {
			setTimeout(()=>{
				endSlide.style.height = `${boxContainerheight.offsetHeight}px`
			},500);
		},
	},


})

//Eventos

swiper.on('slideChange', () =>{
	let current = swiper.activeIndex + 1
	if ( current >= (swiper.slides.length - 1) ){
		
		btnConetionElement.removeAttribute('disabled')
		btnConetionText.textContent = "Conectar a internet gratis"
		btnContainer.classList.add("ani-btn")
	}

})

swiper.init()

window.onload = () => {
	setTimeout( ()=>{
		boxLoad.classList.add("hide-box-fade")
		setTimeout(  ()=>{ 
			boxLoad.style.display = 'none'
			endSlide.style.height = `${boxContainerheight.offsetHeight}px`
		}, 1000)
	},2000)
}