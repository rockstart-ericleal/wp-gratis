(function () {
	'use strict';

	const _$ = (el) => {
		return document.querySelector(el)
	};

	const log = (val) => {
		console.log(val);
		return 0;
	};

	var boxLayerGame = _$('.box-layer-game');
	var lifesUI= 3;

	class UI extends Phaser.Scene {
		constructor(){
			super({key:'UI'});
		}

		init(){
			console.log("Init UI");
			this.scene.moveUp();
		}

		preload(){
			this.load.path = '../../img/assets/game-1/';
			this.load.image('pixel', 'images/pixel.png');
			this.load.image('play', 'images/play-start-ball@2x.png');
			this.load.image('play-yellow', 'images/play-yellow@2x.png');

			//UI
			this.load.image("icon_pausa", "images/icon_pausa.png");
			this.load.image("bg-continue", "images/bg-continue.png");
			//this.load.image("btn_close_ranking", "images/icon_close_ranking.png");

			//UI
			this.load.image('box-point', 'images/box-points.png');
			this.load.image('icon-king', 'images/icon-king.png');
			this.load.image('icon-point', 'images/icon-point.png');
			this.load.image('icon-life', 'images/icon-life.png');

			this.load.image("bg-connect", "images/bg-connect.png");
			this.load.image("ring-load", "images/ring-load.png");

			this.load.image("btn-connect-wifo", "images/btn-connect-wifo.png");

			
			//Sounds
			this.load.audio('fx-click', ['sounds/explosion_shine.mp3']);
		}

		create(){


			let topBoxDistance = 32;
			let box1 = this.add.image(0, 0, 'box-point').setScale(0.5);
			box1.setPosition(box1.displayWidth/2 +10, topBoxDistance);		
			let box2 = this.add.image(box1.x + box1.displayWidth + 10, topBoxDistance, 'box-point').setScale(0.5);
			let box3 = this.add.image(box2.x + box1.displayWidth + 10, topBoxDistance, 'box-point').setScale(0.5);
			// this.add.image(20, 30, 'box-point');

			this.add.image(box1.x - (box1.displayWidth / 2) + 16, box1.y,'icon-king').setScale(0.5);
			this.add.image(box2.x - (box2.displayWidth / 2) + 14, box1.y,'icon-point').setScale(0.5);

			let scoreKing = 123;//boxLayerGame.getAttribute("best-score");

			let scoreStyle = { color: '#000', align: 'right', fontFamily: '"Geogrotesque Medium"' };
			this.scoreKing = this.add.text((box1.x + box1.displayWidth/2) -5, box1.y, scoreKing, scoreStyle);
			this.scoreKing.setOrigin(1, 0.5);

			this.scoreUser = this.add.text((box2.x + box2.displayWidth/2) -5, box2.y, "0", scoreStyle);
			this.scoreUser.setOrigin(1, 0.5);


			this.registry.events.on('set-user-score', (score)=>{
				this.scoreUser.text = score;
			});

			this.registry.events.on('set-current-score', (lifes) => {
				if(lifes > 0){
					let dead = this.lifesGroup.getChildren()[lifes];
					dead.setAlpha(0);
				}
				lifesUI= lifes;
				console.log('LIFES:::', lifes);
			});


			this.lifesGroup = this.add.group();
			

			for (let j = 0; j < lifesUI; j++){
				this.lifesGroup.create(0, box3.y, 'icon-life');
			}

			this.lifesGroup.children.iterate( (life) => {
				life.setScale(0.5);
				
			});

			this.lifesGroup.setX(box3.x - 20, 20);
			this.lifesGroup.setOrigin(0);

			
			//UI Play
			this.clickSound = this.sound.add('fx-click');
			
			this.bgMenu = this.add.image(0, 0, 'pixel');
			this.bgMenu.setScale(this.scale.width*2, this.scale.height*2);
			this.bgMenu.alpha = 0.5;

			

			this.platBg = this.add.image(0, 0, 'play');
			this.platBg.setScale(0.5);

			this.play = this.add.image(0, 0, 'play-yellow');
			this.play.setScale(0.5);
			this.play.setInteractive();

			this.add.tween({
				targets:[this.play],				
				duration:500,
				ease:'Back.easeIn',
				scale: .56,
				yoyo:true,
				repeat: -1
			});



			//Pause

			this.iconPausa = this.add.image(0, 0, 'icon_pausa');
			this.iconPausa.setScale(0.3);
			this.iconPausa.setPosition(this.scale.width-28, 28);
			this.iconPausa.setInteractive();


			this.iconPausa.on(Phaser.Input.Events.POINTER_DOWN, ()=>{

				this.clickSound.play();
				this.add.tween({
					targets:[this.tableroContainer],				
					duration:300,
					ease:'Back.easeIn',
					scale: 1,
					onComplete: () =>{
						this.registry.events.emit('start-superball-pause');
					}
				});
			});


			Phaser.Display.Align.In.Center(this.play, this.platBg, 0, -50);

			this.tableroContainer = this.add.container(this.scale.width/2,this.scale.height/2);
			this.tableroContainer.add([	
				this.bgMenu,		
				this.platBg,
				this.play
			]);

			this.play.on(Phaser.Input.Events.POINTER_DOWN, ()=>{
				this.clickSound.play();
				this.add.tween({
					targets:[this.tableroContainer],				
					duration:300,
					ease:'Back.easeIn',
					scale: 0,
					onComplete: () =>{
						this.registry.events.emit('start-superball');
					}
				});
			});


			if(lifesUI < 3){
				this.tableroContainer.setScale(0);
				this.registry.events.emit('start-superball');	
			}


			//UI Pause Continue

			this.bgMenuRanking = this.add.image(0, 0, 'pixel');
			this.bgMenuRanking.setScale(this.scale.width*2, this.scale.height*2);
			this.bgMenuRanking.alpha = 0.5;		

			this.bgContinue = this.add.image(0, 0, 'bg-continue');
			this.bgContinue.setScale(0.5);

			this.playContinue = this.add.image(0, -54, 'play-yellow');
			this.playContinue.setScale(0.4);
			this.playContinue.setInteractive();




			this.btnConnectWifo = this.add.image(0, 165, 'btn-connect-wifo');
			this.btnConnectWifo.setScale(0.5);
			this.btnConnectWifo.setInteractive();

			this.add.tween({
				targets:[this.playContinue],				
				duration:500,
				ease:'Back.easeIn',
				scale: .48,
				yoyo:true,
				repeat: -1
			});


			let userStyle = {color: '#fff', fontFamily: 'Arial', align: 'center', fontFamily: '"Geogrotesque Medium"', fontSize: '27px', };
			this.nameUser = this.add.text(0,54, 'SANTIAG0', userStyle);
			this.nameUser.setOrigin(0.5);

			let maxScoreStyle = {color: '#fff', fontFamily: 'Arial', align: 'center', fontFamily: '"Geogrotesque Medium"', fontSize: '18px', };
			this.maxScore = this.add.text(0,80, 'TU MEJOR SCORE: 234', maxScoreStyle);
			this.maxScore.setOrigin(0.5);


			this.continueContainer = this.add.container(this.scale.width/2,this.scale.height/2);
			this.continueContainer.add([	
				this.bgMenuRanking,		
				this.bgContinue,
				this.playContinue,
				this.btnConnectWifo,
				this.nameUser,
				this.maxScore
				//this.btn_close_ranking
			]);
			this.continueContainer.setScale(0);


			this.playContinue.on(Phaser.Input.Events.POINTER_DOWN, ()=>{
				this.clickSound.play();
				this.add.tween({
					targets:[this.continueContainer],				
					duration:300,
					ease:'Back.easeIn',
					scale: 0,
					onComplete: () =>{
						this.scene.start('PlayGame');
					}
				});
			});

			this.btnConnectWifo.on(Phaser.Input.Events.POINTER_DOWN, ()=>{
				this.clickSound.play();
				this.add.tween({
					targets:[this.continueContainer],				
					duration:300,
					ease:'Back.easeIn',
					scale: 0,
					onComplete: () =>{

						let scene = this;
						this.time.delayedCall(500, function() {			    				   
						    scene.clickSound.play();
							scene.add.tween({
								targets:[scene.connetContainer],				
								duration:300,
								ease:'Back.easeIn',
								scale: 1,
								onComplete: () =>{
									log("Terminate");
								}
							});
						});				
					}
				});
			});

			



			//UI Connet
			this.bgMenuRing = this.add.image(0, 0, 'pixel');
			this.bgMenuRing.setScale(this.scale.width*2, this.scale.height*2);
			this.bgMenuRing.alpha = 0.5;		

			this.bgRing = this.add.image(0, 0, 'bg-connect');
			this.bgRing.setScale(0.5);

			this.ringLoad = this.add.image(0, 32, 'ring-load');
			this.ringLoad.setScale(0.5);		
			
			this.add.tween({
				targets:[this.ringLoad],				
				duration:6000,
				ease:'Linear',
				rotation: 45,
				repeat:-1,
				onComplete: () =>{
					log("Terminate rotate");
				}
			});


			this.connetContainer = this.add.container(this.scale.width/2,this.scale.height/2);
			this.connetContainer.add([	
				this.bgMenuRing,		
				this.bgRing,
				this.ringLoad
			]);
			this.connetContainer.setScale(0);


			//Events
			this.registry.events.on('connet-superball', ()=> {

				this.clickSound.play();
				this.add.tween({
					targets:[this.connetContainer],				
					duration:300,
					ease:'Back.easeIn',
					scale: 1,
					onComplete: () =>{
						log("Terminate");
					}
				});
			});


			//Events
			this.registry.events.on('show-continue-box', ()=> {
				if(lifesUI > 0 ){
					log('stat dialog continue ball');									
					this.clickSound.play();
					this.add.tween({
						targets:[this.continueContainer],				
						duration:300,
						ease:'Back.easeIn',
						scale: 1,
						onComplete: () =>{
							this.scene.pause('PlayGame');				
						}
					});
				}else{
					this.clickSound.play();
					this.add.tween({
						targets:[this.connetContainer],				
						duration:300,
						ease:'Back.easeIn',
						scale: 1,
						onComplete: () =>{
							log("Terminate");
						}
					});
				}
			});

		}
	}

	const gameOptions = {
		groundPosition: (4 / 6),
		ballHeight: 360,
		//ballGravity: 1500,
		ballGravity: 0,
		ballPosition: (1/4),
		platformSpeed: 650,
		platformDistanceRange: [160, 300],
		platformHeightRange: [0, 40],
		platformLengthRange: [50, 100],
		platformLengthPercent: 50,
		fallingPlatformPercent: 2,
		localStorageName: "score-wg",
		amount_stars: 16
	};

	var boxLayerGame$1 = _$('.box-layer-game');
	// var startSuperball = _$('#start-superball');
	var lifes = 3;

	class PlayGame extends Phaser.Scene{
		constructor(){
			super("PlayGame");
		}

		init(){
				
		}

		preload(){

			//Images
			this.load.path = '../../img/assets/game-1/';

			this.load.image("ground", "images/ground.png");
			this.load.image("bgg", "images/bgg.png");
			this.load.image("bg-ad", "ads/ads-1.jpg");
			
			this.load.image('blue', './particles/blue_.png');
			this.load.image('explo-fire', 'particles/fire.png');
			this.load.image("ball", "images/ball.png");
			this.load.image("star", "images/stars.png");
			this.load.image("fire", "images/fire.png");
			//this.load.image("icon_king", "images/icon_king.png");
			
			//Sounds
			this.load.audio('fx-bounce', ['sounds/bounce-ball-1.mp3']);
			this.load.audio('fx-sound-bg', ['sounds/sound-bg.mp3']);
			this.load.audio('fx-fire', ['sounds/incident_game.mp3']);

			//Fonts
			this.load.image('number-font', 'fonts/number-font.png');
			this.load.json('number-font-json','fonts/number-font.json');

			//Ads
			this.load.image('add-1', 'ads/ads-1.jpg');

			



			
			
		}

		create(){

			
			this.templateScore = '000000';	

			//
			this.soundFireActive = false;
			let rnd = new Phaser.Math.RandomDataGenerator((Date.now() * Math.random()).toString());
			this.sound.stopAll();
			//Cameras
			this.camera = this.cameras.main;

			//Sounds
			this.bounceSound = this.sound.add('fx-bounce');
			this.bounceSound.volume = 0.7;
			
			// this.bgSound = this.sound.add('fx-sound-bg');
			// this.bgSound.loop = true;
			// this.bgSound.play();
			// this.bgSound.volume = 0.2;

			this.fireSound = this.sound.add('fx-fire');		

			//Create Group which will contain all platforms
			this.platformGroup = this.physics.add.group();
			this.fireGroup = this.add.group();
			
			this.bgg = this.add.image(0, 0, 'bgg');
			this.bgAd = this.add.image(0, 0,'bg-ad');
			this.bgAd.setDisplaySize(this.scale.width, this.scale.height);
			this.bgAd.setPosition(this.scale.width/2, this.scale.height/2);
			this.bgAd.alpha = 0.8;

			this.starsArray = [];
			for(let s = 0; s < gameOptions.amount_stars; s++){
				let xStar = Phaser.Math.Between(this.game.config.width + 100, window.innerWidth);
				let yStar = Phaser.Math.Between(100, this.game.config.height*2 -30);

				this.starBg = this.add.sprite(xStar, yStar, 'star');
				this.starBg.velocity = 0.2 + rnd.frac() * 2;
				this.starBg.alpha = rnd.frac() - 0.3;
				this.starBg.scale = (0.3 + rnd.frac());
				this.starsArray[s] = this.starBg;
			}

	       
			this.bgg.setDisplaySize(this.game.config.width*2,this.game.config.height*2);

			this.ball = this.physics.add.image(this.game.config.width * gameOptions.ballPosition, this.game.config.height * gameOptions.groundPosition - gameOptions.ballHeight, "ball");
			this.ball.body.gravity.y = gameOptions.ballGravity;
			this.ball.setBounce(1);
			this.ball.setScale(0.7);
			this.ball.body.checkCollision.down = true;
	        this.ball.body.checkCollision.up = false;
	        this.ball.body.checkCollision.left = false;
	        this.ball.body.checkCollision.right = false;
	        this.ball.setSize(30, 50, true);
	        this.ball.body.setCircle(18);
	        this.ball.body.setOffset(18, 15);

	        //First Platform 
	        let platformX = this.ball.x;

	        for ( let i = 0; i < 5; i++ ){
	        	let platform = this.platformGroup.create(0, 0, "ground" );
	        	platform.setImmovable(true);
	        	platform.body.setSize(platform.width-20, 50);
	        	platform.body.setOffset(10, 10);
	        	platform.setScale(0.5);

	        	this.placePlatform(i, platform, platformX);
	        	platformX += Phaser.Math.Between(gameOptions.platformDistanceRange[0], gameOptions.platformDistanceRange[1]);
	        }

	        console.log('reload=> ',this.platformGroup);

	        this.input.on("pointerdown", this.movePlatforms, this);
	        this.input.on("pointerup", this.stopPlatforms, this);

	        this.score = 0;

	        //NO FUNCIONA EN PORTAL CAUTIVO
	        this.topScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : localStorage.getItem(gameOptions.localStorageName);

	        this.scoreText = this.add.text(10, 10, "");     
	        
	        let particles = this.add.particles('blue');
	        let emitter = particles.createEmitter({
		        speed: 50,
		        scale: { start: .4, end: 0},
		        blendMode: 'ADD',
		        gravityY: 350,
		        lifespan: 500
			});
		    emitter.startFollow(this.ball);


		    let exploFire = this.add.particles('explo-fire');
		    this.explotion = exploFire.createEmitter({
		    	active:false, 
		    	yoyo: false, 
		    	quantity: 1,
		    	scale: { start: .8, end: 0},
		    });
		    this.explotion.setLifespan(600);
		    this.explotion.setSpeed(200);

			//Events
			this.registry.events.on('start-superball', ()=> {
				log('stat super ball');
				this.ball.body.gravity.y = 1000;
				this.scene.resume('PlayGame');
			});

			this.registry.events.on('start-superball-pause', ()=> {
				log('stat super ball pause');
				this.scene.pause('PlayGame');
			});

			this.scene.launch('UI');

			if(lifes <= 0){
				this.scene.pause('PlayGame');	
				this.registry.events.emit('connet-superball');
			}


			//this.registry.events.emit('set-current-score', lifes);
			//this.updateScore(0);
					
		}

		startGame(){
			//this.scene.start("PlayGame");
		}

		update(){
			let c = this.camera;
			let bs = this.bounceSound;



			this.physics.world.collide(this.platformGroup, this.ball, function(platform, ball){
	        	if(ball.fallingDown){
	        		ball.body.gravity.y = 1000;
	        	}
	        	c.shake(50, 0.01);
	        	bs.play();	
	        	// log(platform.body.bounce.y, this);
	        });



	        this.platformGroup.getChildren().forEach(function(platform){
	        	if (platform.getBounds().right < 0 ){
	        		this.updateScore(1);
	        		this.placePlatform(1,platform, this.getRightmostPlatform() + Phaser.Math.Between(gameOptions.platformDistanceRange[0], gameOptions.platformDistanceRange[1]) );
	        	}
	        }, this);
	     
	        if(this.ball.y > this.game.config.height){         
	           	
	          	if (this.soundFireActive == false){
	          		this.fireSound.play();
	          		//NO FUNCIONA EN PORTAL CAUTIVO
		            localStorage.setItem(gameOptions.localStorageName, Math.max(this.score, this.topScore));         
		            this.explotion.setBlendMode(Phaser.BlendModes.ADD);
		            this.explotion.setPosition(this.ball.x, this.game.config.height);
		            this.explotion.active = true;

		            let e = this.explotion;
		   			let es = this;   		
		   			

		   			
		   			lifes = lifes - 1;
		   			this.registry.events.emit('set-current-score', lifes);

		            this.time.delayedCall(1600, function() {			    				   
					    es.registry.events.emit('show-continue-box');
					    //es.start("PlayGame");
					});
	          	}
	          	this.soundFireActive = true;

	            // this.explotion.tint(0xff0000);   			
	         
	        }


	        for(let s = 0; s < gameOptions.amount_stars; s++){						
				let starBg = this.starsArray[s];
				starBg.x -= starBg.velocity;
				if(starBg.x <= -100){
					starBg.x = Phaser.Math.Between(window.innerWidth + 100, window.innerWidth);
					starBg.y = Phaser.Math.Between(100, window.innerHeight*2 -30);
				}
				//log(starBg.x);
			}

		}

		getRightmostPlatform(){
			let rightmostPlatform = 0;
			this.platformGroup.getChildren().forEach(function(platform){
				rightmostPlatform = Math.max(rightmostPlatform, platform.x);
			}, this);
			return rightmostPlatform;
		}

		updateScore(currentScore){
			this.score += currentScore;
			this.registry.events.emit('set-user-score', this.score);
		}

		movePlatforms(event){
			if(event.y >= 100){
				this.platformGroup.setVelocityX(-gameOptions.platformSpeed);	
			}
		}

		stopPlatforms(){
			this.platformGroup.setVelocityX(0);
		}

		placePlatform(index, platform, posX){
			platform.x = posX;
			//Variante de altura de plataformas.
			platform.y = this.game.config.height * gameOptions.groundPosition + Phaser.Math.Between(gameOptions.platformHeightRange[0], gameOptions.platformHeightRange[1]);

			//Permitir caer a la plataforma
			log(posX != this.ball.x && Phaser.Math.Between(1, 100) <= gameOptions.fallingPlatformPercent);
			//platform.fallingDown = posX != this.ball.x && Phaser.Math.Between(1, 100) <= gameOptions.fallingPlatformPercent;
			log('INDEX');
			if(index == 0){
				platform.fallingDown = false;
			}else{
				platform.fallingDown = false;
			}

			platform.displayWidth = Phaser.Math.Between(gameOptions.platformLengthRange[0], gameOptions.platformLengthRange[1]);

			platform.body.gravity.y = 0;
			platform.body.velocity.y = 0;
		}

	}

	// Local vars
	let containerGame = _$('.container-box-game');
	let linkGameSuperBall = _$('#game-super-ball');
	let boxLayerGame$2 = _$('.box-layer-game');


	linkGameSuperBall.addEventListener('click', () => {
		 containerGame.style.display = 'inline-block';
		// boxLayerGame.style.display = 'flex';
		// boxLayerGame.classList.add('ani-bounce');

		try {		
			let config = {
				type: Phaser.AUTO,
				backgroundColor: 0x022651,
				scale: {
					mode: Phaser.Scale.FIT,
					autoCenter: Phaser.Scale.CENTER_BOTH,
					parent: "container-game",
					width: window.innerWidth,
					height: window.innerHeight,	
				},
				physics: {
					default: "arcade",
					arcade:{
						// gravity: {
						// 	y:800
						// },
						// debug: true,
					}
				},
				scene: [PlayGame, UI]
			};

			const game = new Phaser.Game(config);
			window.focus();
			

		}
		catch(error) {
		  
		  let el = document.querySelector("body");
		  el.innerHTML = 'err + ' + error;
		}
	});


		


	window.onerror = function(errorMsg, url, lineNumber) {
		console.log(errorMsg,url,lineNumber);
	    let el = document.querySelector("body");
		el.innerHTML = '<h5>'+errorMsg+'</h5><a href="'+url+'">'+url+'</a><h5>'+lineNumber+'</h5>';
	};


	let boxLoad = _$(".box-wait");
	window.onload = () => {
		
		setTimeout( ()=>{
			boxLoad.classList.add("hide-box-fade");
			setTimeout(  ()=>{ boxLoad.style.display = 'none'; }, 1000);
		},2000);

	};

}());
